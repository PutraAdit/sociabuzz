-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2018 at 02:04 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sociatest`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `action_date` datetime NOT NULL,
  `params_action` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `username`, `action_date`, `params_action`, `created_at`, `updated_at`) VALUES
(1, 'Clifton', '2018-03-14 12:55:17', 'Login to Dashboard', '2018-03-14 11:55:17', NULL),
(2, 'Clifton', '2018-03-14 12:55:17', 'Visit index/dashboard', '2018-03-14 11:55:17', NULL),
(3, 'Clifton', '2018-03-14 12:55:22', 'Visit create page', '2018-03-14 11:55:22', NULL),
(4, 'Clifton', '2018-03-14 12:55:28', 'Insert order with order code : ORD-\'1516989\'', '2018-03-14 11:55:28', NULL),
(5, 'Clifton', '2018-03-14 12:55:28', 'Visit index/dashboard', '2018-03-14 11:55:28', NULL),
(6, 'Clifton', '2018-03-14 12:58:49', 'Visit index/dashboard', '2018-03-14 11:58:49', NULL),
(7, 'Clifton', '2018-03-14 12:58:59', 'Visit create page', '2018-03-14 11:58:59', NULL),
(8, 'Clifton', '2018-03-14 12:59:51', 'Visit create page', '2018-03-14 11:59:51', NULL),
(9, 'Clifton', '2018-03-14 13:00:11', 'Visit create page', '2018-03-14 12:00:11', NULL),
(10, 'Clifton', '2018-03-14 13:02:15', 'Visit create page', '2018-03-14 12:02:15', NULL),
(11, 'Clifton', '2018-03-14 13:07:15', 'Visit create page', '2018-03-14 12:07:15', NULL),
(12, 'Clifton', '2018-03-14 13:09:15', 'Visit create page', '2018-03-14 12:09:15', NULL),
(13, 'Clifton', '2018-03-14 13:10:12', 'Visit create page', '2018-03-14 12:10:12', NULL),
(14, 'Clifton', '2018-03-14 13:10:29', 'Visit create page', '2018-03-14 12:10:29', NULL),
(15, 'Clifton', '2018-03-14 13:10:43', 'Visit create page', '2018-03-14 12:10:43', NULL),
(16, 'Clifton', '2018-03-14 13:13:43', 'Visit create page', '2018-03-14 12:13:43', NULL),
(17, 'Clifton', '2018-03-14 13:15:20', 'Visit index/dashboard', '2018-03-14 12:15:20', NULL),
(18, 'Clifton', '2018-03-14 13:15:25', 'Logout from Application', '2018-03-14 12:15:25', NULL),
(19, 'Thelma', '2018-03-14 13:15:38', 'Login to Dashboard', '2018-03-14 12:15:38', NULL),
(20, 'Thelma', '2018-03-14 13:15:38', 'Visit index/dashboard', '2018-03-14 12:15:38', NULL),
(21, 'Thelma', '2018-03-14 13:15:44', 'Visit edit page', '2018-03-14 12:15:44', NULL),
(22, 'Thelma', '2018-03-14 13:15:59', 'Update order with order code : \'ORD-1534484\'', '2018-03-14 12:15:59', NULL),
(23, 'Thelma', '2018-03-14 13:15:59', 'Visit index/dashboard', '2018-03-14 12:15:59', NULL),
(24, 'Thelma', '2018-03-14 13:16:08', 'Visit create page', '2018-03-14 12:16:08', NULL),
(25, 'Thelma', '2018-03-14 13:16:13', 'Visit index/dashboard', '2018-03-14 12:16:13', NULL),
(26, 'Thelma', '2018-03-14 13:16:22', 'Visit edit page', '2018-03-14 12:16:22', NULL),
(27, 'Thelma', '2018-03-14 13:16:29', 'Update order with order code : \'ORD-1516989\'', '2018-03-14 12:16:29', NULL),
(28, 'Thelma', '2018-03-14 13:16:29', 'Visit index/dashboard', '2018-03-14 12:16:29', NULL),
(29, 'Thelma', '2018-03-14 13:16:39', 'Visit create page', '2018-03-14 12:16:39', NULL),
(30, 'Thelma', '2018-03-14 13:16:44', 'Visit index/dashboard', '2018-03-14 12:16:44', NULL),
(31, 'Thelma', '2018-03-14 13:24:46', 'Visit edit page', '2018-03-14 12:24:46', NULL),
(32, 'Thelma', '2018-03-14 13:25:47', 'Visit edit page', '2018-03-14 12:25:47', NULL),
(33, 'Thelma', '2018-03-14 13:42:59', 'Visit edit page', '2018-03-14 12:42:59', NULL),
(34, 'Thelma', '2018-03-14 13:43:02', 'Visit edit page', '2018-03-14 12:43:02', NULL),
(35, 'Thelma', '2018-03-14 13:43:45', 'Visit edit page', '2018-03-14 12:43:45', NULL),
(36, 'Thelma', '2018-03-14 13:43:47', 'Visit edit page', '2018-03-14 12:43:47', NULL),
(37, 'Thelma', '2018-03-14 13:44:39', 'Visit edit page', '2018-03-14 12:44:39', NULL),
(38, 'Thelma', '2018-03-14 13:46:39', 'Visit edit page', '2018-03-14 12:46:39', NULL),
(39, 'Thelma', '2018-03-14 13:46:46', 'Update order with order code : \'\'', '2018-03-14 12:46:46', NULL),
(40, 'Thelma', '2018-03-14 13:46:46', 'Visit index/dashboard', '2018-03-14 12:46:46', NULL),
(41, 'Thelma', '2018-03-14 13:46:54', 'Visit edit page', '2018-03-14 12:46:54', NULL),
(42, 'Thelma', '2018-03-14 13:47:05', 'Update order with order code : \'\'', '2018-03-14 12:47:05', NULL),
(43, 'Thelma', '2018-03-14 13:47:05', 'Visit index/dashboard', '2018-03-14 12:47:05', NULL),
(44, 'Thelma', '2018-03-14 13:47:57', 'Visit index/dashboard', '2018-03-14 12:47:57', NULL),
(45, 'Thelma', '2018-03-14 13:48:00', 'Visit edit page', '2018-03-14 12:48:00', NULL),
(46, 'Thelma', '2018-03-14 13:48:52', 'Visit edit page', '2018-03-14 12:48:52', NULL),
(47, 'Thelma', '2018-03-14 13:48:54', 'Visit edit page', '2018-03-14 12:48:54', NULL),
(48, 'Thelma', '2018-03-14 13:49:17', 'Visit edit page', '2018-03-14 12:49:17', NULL),
(49, 'Thelma', '2018-03-14 13:49:18', 'Visit edit page', '2018-03-14 12:49:18', NULL),
(50, 'Thelma', '2018-03-14 13:49:26', 'Visit index/dashboard', '2018-03-14 12:49:26', NULL),
(51, 'Thelma', '2018-03-14 13:49:30', 'Visit edit page', '2018-03-14 12:49:30', NULL),
(52, 'Thelma', '2018-03-14 13:49:42', 'Visit index/dashboard', '2018-03-14 12:49:42', NULL),
(53, 'Thelma', '2018-03-14 13:49:44', 'Visit edit page', '2018-03-14 12:49:44', NULL),
(54, 'Thelma', '2018-03-14 13:51:54', 'Visit edit page', '2018-03-14 12:51:54', NULL),
(55, 'Thelma', '2018-03-14 13:51:56', 'Visit edit page', '2018-03-14 12:51:56', NULL),
(56, 'Thelma', '2018-03-14 13:52:05', 'Visit edit page', '2018-03-14 12:52:05', NULL),
(57, 'Thelma', '2018-03-14 13:52:47', 'Visit edit page', '2018-03-14 12:52:47', NULL),
(58, 'Thelma', '2018-03-14 13:52:58', 'Visit edit page', '2018-03-14 12:52:58', NULL),
(59, 'Thelma', '2018-03-14 13:53:28', 'Visit edit page', '2018-03-14 12:53:28', NULL),
(60, 'Thelma', '2018-03-14 13:53:34', 'Visit index/dashboard', '2018-03-14 12:53:34', NULL),
(61, 'Thelma', '2018-03-14 13:54:37', 'Visit index/dashboard', '2018-03-14 12:54:37', NULL),
(62, 'Thelma', '2018-03-14 13:54:39', 'Visit edit page', '2018-03-14 12:54:39', NULL),
(63, 'Thelma', '2018-03-14 13:56:04', 'Visit edit page', '2018-03-14 12:56:04', NULL),
(64, 'Thelma', '2018-03-14 13:59:32', 'Visit edit page', '2018-03-14 12:59:32', NULL),
(65, 'Thelma', '2018-03-14 13:59:41', 'Visit index/dashboard', '2018-03-14 12:59:41', NULL),
(66, 'Thelma', '2018-03-14 13:59:44', 'Visit edit page', '2018-03-14 12:59:44', NULL),
(67, 'Thelma', '2018-03-14 14:00:30', 'Update order with order code : \'ORD-9256559\'', '2018-03-14 13:00:30', NULL),
(68, 'Thelma', '2018-03-14 14:00:31', 'Visit index/dashboard', '2018-03-14 13:00:31', NULL),
(69, 'Thelma', '2018-03-14 14:00:39', 'Visit edit page', '2018-03-14 13:00:39', NULL),
(70, 'Thelma', '2018-03-14 14:00:48', 'Visit index/dashboard', '2018-03-14 13:00:48', NULL),
(71, 'Thelma', '2018-03-14 14:00:50', 'Visit edit page', '2018-03-14 13:00:50', NULL),
(72, 'Thelma', '2018-03-14 14:00:55', 'Visit index/dashboard', '2018-03-14 13:00:55', NULL),
(73, 'Thelma', '2018-03-14 14:00:57', 'Visit edit page', '2018-03-14 13:00:57', NULL),
(74, 'Thelma', '2018-03-14 14:01:04', 'Update order with order code : \'ORD-6209719\'', '2018-03-14 13:01:04', NULL),
(75, 'Thelma', '2018-03-14 14:01:04', 'Visit index/dashboard', '2018-03-14 13:01:04', NULL),
(76, 'Thelma', '2018-03-14 14:01:08', 'View order with order ID : \'21\'', '2018-03-14 13:01:08', NULL),
(77, 'Thelma', '2018-03-14 14:01:10', 'Visit index/dashboard', '2018-03-14 13:01:10', NULL),
(78, 'Thelma', '2018-03-14 14:01:12', 'Visit edit page', '2018-03-14 13:01:12', NULL),
(79, 'Thelma', '2018-03-14 14:01:22', 'Visit index/dashboard', '2018-03-14 13:01:22', NULL),
(80, 'Thelma', '2018-03-14 14:01:47', 'Visit edit page', '2018-03-14 13:01:47', NULL),
(81, 'Thelma', '2018-03-14 14:01:54', 'Update order with order code : \'ORD-4801438\'', '2018-03-14 13:01:54', NULL),
(82, 'Thelma', '2018-03-14 14:01:54', 'Visit index/dashboard', '2018-03-14 13:01:54', NULL),
(83, 'Thelma', '2018-03-14 14:02:16', 'Visit create page', '2018-03-14 13:02:16', NULL),
(84, 'Thelma', '2018-03-14 14:02:28', 'Insert order with order code : ORD-\'2218062\'', '2018-03-14 13:02:28', NULL),
(85, 'Thelma', '2018-03-14 14:02:28', 'Visit index/dashboard', '2018-03-14 13:02:28', NULL),
(86, 'Thelma', '2018-03-14 14:02:32', 'Visit edit page', '2018-03-14 13:02:32', NULL),
(87, 'Thelma', '2018-03-14 14:03:32', 'Visit edit page', '2018-03-14 13:03:32', NULL),
(88, 'Thelma', '2018-03-14 14:03:49', 'Logout from Application', '2018-03-14 13:03:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Pizza Deluxe', 150000, 1, '2018-03-13 14:28:41', NULL),
(2, 'Tahu Crispy', 20000, 1, '2018-03-13 14:28:41', NULL),
(3, 'Spaghetti', 40000, 1, '2018-03-13 14:29:34', NULL),
(4, 'Capuccino', 15000, 1, '2018-03-13 14:29:34', NULL),
(5, 'Americano', 13000, 1, '2018-03-13 14:29:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(5);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) UNSIGNED NOT NULL,
  `order_code` varchar(100) NOT NULL,
  `total_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `table_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `total_order`, `status`, `table_id`, `user_id`, `created_at`, `updated_at`) VALUES
(21, 'ORD-6209719', 90000, 1, 0, 4, '2018-03-14 07:46:07', NULL),
(22, 'ORD-1534484', 60000, 1, 2, 1, '2018-03-14 07:46:59', NULL),
(23, 'ORD-4801438', 55000, 1, 3, 2, '2018-03-14 08:30:12', NULL),
(24, 'ORD-9256559', 203000, 1, 2, 5, '2018-03-14 08:31:56', NULL),
(25, 'ORD-1516989', 238000, 1, 2, 1, '2018-03-14 11:55:28', NULL),
(26, 'ORD-2218062', 60000, 0, 1, 4, '2018-03-14 13:02:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `item_id`, `created_at`, `updated_at`) VALUES
(7, '22', 2, '2018-03-14 07:46:59', NULL),
(8, '21', 2, '2018-03-14 07:49:28', NULL),
(9, '21', 3, '2018-03-14 07:49:28', NULL),
(10, '21', 4, '2018-03-14 07:49:44', NULL),
(11, '23', 3, '2018-03-14 08:30:12', NULL),
(12, '23', 4, '2018-03-14 08:30:12', NULL),
(13, '24', 1, '2018-03-14 08:31:57', NULL),
(14, '24', 5, '2018-03-14 08:31:57', NULL),
(15, '24', 3, '2018-03-14 08:33:27', NULL),
(16, '26', 2, '2018-03-14 13:02:28', NULL),
(17, '26', 3, '2018-03-14 13:02:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `id` int(11) UNSIGNED NOT NULL,
  `table_no` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`id`, `table_no`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, '2018-03-13 14:30:24', NULL, 0),
(2, 2, '2018-03-13 14:30:24', NULL, 1),
(3, 3, '2018-03-13 14:30:31', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `created_at`, `updated_at`, `username`, `email`, `password`, `role`) VALUES
(1, 'Thelma', '2018-03-13 14:23:19', NULL, 'xbernier', 'cristian.rowe@naderkris.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Kasir'),
(2, 'Clifton', '2018-03-13 14:23:19', NULL, 'hbreitenberg', 'wgreenholt@yahoo.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Waiter'),
(3, 'Eulalia', '2018-03-13 14:23:19', NULL, 'grover.bergnaum', 'cpagac@yahoo.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Waiter'),
(4, 'Adriel', '2018-03-13 14:23:19', NULL, 'tgorczany', 'wgibson@robel.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Waiter'),
(5, 'Velva', '2018-03-13 14:23:19', NULL, 'keaton.reinger', 'ddouglas@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Waiter'),
(6, 'Admin', '2018-03-14 10:47:13', NULL, 'Admin Manager', 'admin@example.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
