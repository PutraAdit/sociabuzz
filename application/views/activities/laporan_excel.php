<?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$title.xls"); 
    header("Pragma: no-cache");
    header("Expires: 0");
?>
 
<table class="table table-bordered">
    <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Visit</th>
        <th>Message</th>
    </tr>
    <?php 
    if ($data) {
        $no = 1;
        foreach ($data as $key => $result) {         
    ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $result->username; ?></td>
            <td><?php echo $result->action_date; ?></td>
            <td><?php echo $result->params_action; ?></td>
        </tr>
    <?php 
        $no++;
        }
    }
    ?>
</table>

