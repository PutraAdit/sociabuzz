<div class="col-md-9">
	<div class="table-responsive">
		<a href="<?php echo base_url(); ?>index.php/activity/export_excel" class="btn btn-success"> Download Laporan</a><br><br>
		<table class="table table-bordered">
			<tr>
				<th>No.</th>
				<th>Name</th>
				<th>Visit</th>
				<th>Message</th>
			</tr>
			<?php 
			if ($data) {
				$no = 1;
				foreach ($data as $key => $result) {
			
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $result->username; ?></td>
				<td><?php echo $result->action_date; ?></td>
				<td><?php echo $result->params_action; ?></td>
			</tr>
			<?php 
				$no++;
				}
			}
			?>
		</table>
	</div>
</div>