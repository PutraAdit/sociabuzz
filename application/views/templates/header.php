<!DOCTYPE html>
<html>
<head>
    <title>SociaBuzz Test</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets//js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">SociaBuzz Test</a>
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url(); ?>index.php/order/dashboard">Order</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/activity/dashboard">Activity</a></li>
      </ul>
    </div>
    <?php if ($this->session->userdata('status') == 'login') { ?>
    <div class="pull-right" style="margin-top: 12px;">
      <a class="btn btn-danger btn-xs" href="<?php print base_url();?>index.php/user/logout"><i class="fa fa-mail-user"></i> Logout</a>
      </div>
    <?php } ?>
  </div>
</nav>
<div class="container">
    <div class="row">