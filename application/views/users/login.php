<?php
$this->load->view('templates/header');
?>

<header class="page-header">
    <h1 class="entry-title">Login Form</h1>
</header>   
<div class="row page-content">
	<div class="col-lg-12">		
		<?php if(validation_errors()) { ?>
			<div class="alert alert-danger">
				<?php echo validation_errors(); ?>
			</div>
		<?php } ?>
		<?php if(!empty($this->input->get('msg')) && $this->input->get('msg') == 1) { ?>
			<div class="alert alert-danger">
				Please Enter Your Valid Information.
			</div>
		<?php } ?>
		<?php echo form_open('user/dologin'); ?>
					
			<div class="form-group">
			   <input type="text" name="email" class="form-control" id="email" placeholder="Email">
			</div>
			<div class="form-group">
			   <input type="password" name="password" class="form-control" id="password" placeholder="Password">
			</div>	
			<div class="form-group pull-right">
		   <button type="submit" id="login" class="btn btn-primary">Login</button>
		</div>
			
		</div>
		
		<?php echo form_close(); ?>
	
</div>
<footer class="entry-meta">
	<span class="edit-link">
		<div style="font-size: 12px;"><strong>Note:</strong><br><br>
			<p>Login Waiter: wgreenholt@yahoo.com Password: 12345</p>
			<p>Login Kasir: cristian.rowe@naderkris.com Password: 12345</p>
			<p>Login Manager: admin@example.com Password: 12345</p>
		</div>
	</span>	
</footer>
<?php
$this->load->view('templates/footer');
?>
