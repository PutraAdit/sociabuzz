<div class="col-md-9">
	<div class="table-responsive">
		<a href="<?php echo base_url(); ?>index.php/order/create" class="btn btn-success"> Create</a>
		<a href="<?php echo base_url(); ?>index.php/order/export_excel" class="btn btn-success"> Download Laporan</a><br><br>
		<table class="table table-bordered">
			<tr>
				<th>No.</th>
				<th>Kode Order</th>
				<th>Penerima</th>
				<th>No Meja</th>
				<th>Total</th>
				<th>Status</th>
				<th>Aksi</th>
			</tr>
			<?php 
			if ($data) {
				$no = 1;
				foreach ($data as $key => $result) {
			
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $result->order_code; ?></td>
				<td><?php echo $result->uname; ?></td>
				<td><?php echo $result->table; ?></td>
				<td><?php echo $result->total; ?></td>
				<td>
					<?php 
						if ($result->stat == 0) {
							echo "Belum Dibayar";
						} else {
							echo "Lunas";
						}
					?>
				</td>
				<td>
					<?php if ($this->session->userdata('role') == 'Manager') { ?>
						<a href="<?php echo base_url(); ?>index.php/order/hapus/<?php echo $result->id; ?>" class="btn btn-danger"> Hapus </a>
					<?php } ?>
					<?php if ($result->stat == 0) { ?>
						<a href="<?php echo base_url(); ?>index.php/order/edit/<?php echo $result->id; ?>" class="btn btn-success"> Edit</a>
					<?php } ?>
					<a href="<?php echo base_url(); ?>index.php/order/show/<?php echo $result->id; ?>" class="btn btn-default"> Show</a>
				</td>
			</tr>
			<?php 
				$no++;
				}
			}
			?>
		</table>
	</div>
</div>