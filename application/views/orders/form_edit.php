<?php 
$result = $data_order[0];

?>

<div class="col-md-5">
	<form method="post" action="<?php echo base_url(); ?>index.php/order/update">
		<input type="hidden" name="id" value="<?php echo $result->id; ?>">
		<div class="form-group">
					<label>Kode Order</label>
					<input type="text" name="order_code" class="form-control" value="<?php echo $result->order_code; ?>" readonly>		
				</div>
				<div class="form-group">
					<label>Total Order</label>
					<input type="text" name="total_order" class="form-control" value="<?php echo $result->total_order; ?>">
				</div>
				<div class="form-group">
					<label>Menu</label>
					<select name="menu_id[]" class="form-control" id="" data-placeholder="" multiple>                           
					    <?php foreach($menu as $key => $value) { ?>
					        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
					    <?php }; ?>
					</select>
				</div>
				<div class="form-group">
					<label>No Meja : </label>
					<?php foreach($data2 as $key => $value) { ?>
						<?php if ($value->id == $result->table_id) { ?>
						    <input type="hidden" name="table_id" class="form-control" value="<?php echo $value->id; ?>">
					     	<span><?php echo $value->table_no; ?></span>
					    <?php } ?>
					<?php }; ?>
				</div>
				<div class="form-group">
					<label>Penerima : </label>
				    <?php foreach($user as $key => $value) { ?>
				        <?php if ($value->id == $result->user_id) { ?>
				        	<input type="hidden" name="user_id" class="form-control" value="<?php echo $value->id; ?>">
				        	<span><?php echo $value->name; ?></span>
				        <?php } ?>
				    <?php }; ?>
				</div>
				<div class="form-group">
					<label>Pesanan Saat Ini : </label>
					<span><?php echo $order_detail ?></span>
				</div>
				<?php if ($this->session->userdata('role') == 'Kasir') { ?>
					<div class="form-group">
						<label>Dibayar : </label>
						<select name="status" class="form-control" id="" data-placeholder="">                           
						    <?php foreach($status as $key => $value) { ?>
						        <option <?php if($key == $result->status){ echo 'selected="selected"'; } ?> value="<?php echo "{$key}"; ?>"><?php echo $value[$key]; ?></option>
						    <?php }; ?>
						</select>
					</div>
				<?php } ?>
 				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
					<a href='#myModal' class='btn btn-default btn-small' id='custId' data-toggle='modal' data-id=".$result->id.">Change Table</a></td>
				</div>
	</form>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-xs">
      	<div class="modal-content">
        	<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title text-center">Change Table</h4>
        	</div>
          	<div class="modal-body"><br>
	           	<form  action="<?php echo base_url(); ?>index.php/order/update_table" method="POST">
	           		<input type="hidden" name="id" value="<?php echo $result->id; ?>">
	                <div class="form-group">
					<label>No Meja</label>
					<select name="table_id" class="form-control" id="" data-placeholder="">                           
					    <?php foreach($data as $key => $value) { ?>
					        <option <?php if($value->id == $result->table_id){ echo 'selected="selected"'; } ?> value="<?php echo $value->id; ?>"><?php echo $value->table_no; ?></option>
					    <?php }; ?>
					</select>
				</div>			
				 	<div class="modal-footer">  		
				 		<button type="submit" name="Submit" class="btn btn-info m" >Save</button>
	                	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        		</div>
	            </form>
        	</div>
    	</div>
	</div>
</div>

