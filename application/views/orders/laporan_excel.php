<?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$title.xls"); 
    header("Pragma: no-cache");
    header("Expires: 0");
?>
 
<table class="table table-bordered">
    <tr>
        <th>No.</th>
        <th>Kode Order</th>
        <th>Penerima</th>
        <th>No Meja</th>
        <th>Total</th>
        <th>Status</th>
    </tr>
    <?php 
        if ($data) {
            $no = 1;
            foreach ($data as $key => $result) {
            
    ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $result->order_code; ?></td>
            <td><?php echo $result->uname; ?></td>
            <td><?php echo $result->table; ?></td>
            <td><?php echo $result->total; ?></td>
            <td>
                <?php 
                    if ($result->stat == 0) {
                        echo "Belum Dibayar";
                    } else {
                        echo "Lunas";
                    }
                ?>
            </td>
        </tr>
        <?php 
            $no++;
            }
        }
            ?>
        </table>

