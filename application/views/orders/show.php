<!DOCTYPE html>
<html>
<head>
	<title>SociaBuzz Test</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets//js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">SociaBuzz Test</a>
    </div>
  </div>
</nav>
<div class="container">
	<div class="row">
		<div class="col-md-6">
				<?php foreach ($data_order as $key => $value) { ?>
				<div class="form-group">
					<label>Kode Order : </label>
					<span><?php echo $value->order_code; ?></span>		
				</div>
				<div class="form-group">
					<label>Total Order : </label>
					<span><?php echo $value->total; ?></span>		
				</div>
				<div class="form-group">
					<label>Penerima : </label>
					<span><?php echo $value->uname; ?></span>		
				</div>
				<div class="form-group">
					<label>No Meja : </label>
					<span><?php echo $value->table; ?></span>		
				</div>
				<div class="form-group">
					<label>Pesanan : </label>
					<span><?php echo $order_detail; ?></span>		
				</div>
				<div class="form-group">
					<label>Status : </label>
					<span>
						<?php 
							if ($value->stat == 0) {
								echo "Belum Dibayar";
							} else {
								echo "Lunas";
								} 
						?>
					</span>		
				</div>
				<?php } ?>
		</div>
	</div>
</div>

</body>
</html>