<!DOCTYPE html>
<html>
<head>
	<title>SociaBuzz Test</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets//js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">SociaBuzz Test</a>
    </div>
  </div>
</nav>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<form method="post" action="<?php echo base_url(); ?>index.php/order/insert">
				<div class="form-group">
					<label>Kode Order</label>
					<input type="text" name="order_code" class="form-control" value="<?php echo $code; ?>" readonly>		
				</div>
				<div class="form-group">
					<label>No Meja</label>
					<select name="table_id" class="form-control" id="" data-placeholder="">                           
					    <?php foreach($data as $key => $value) { ?>
					        <option value="<?php echo $value->id; ?>"><?php echo $value->table_no; ?></option>
					    <?php }; ?>
					</select>
				</div>
				<div class="form-group">
					<label>Menu</label>
					<select name="menu_id[]" class="form-control" id="" data-placeholder="" multiple>                           
					    <?php foreach($menu as $key => $value) { ?>
					        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
					    <?php }; ?>
					</select>
				</div>
				<div class="form-group">
					<label>Penerima</label>
					<select name="user_id" class="form-control" id="" data-placeholder="">                           
					    <?php foreach($user as $key => $value) { ?>
					        <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
					    <?php }; ?>
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Insert</button>
				</div>
			</form>
		</div>
	</div>
</div>

</body>
</html>