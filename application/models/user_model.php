<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	private $_userID;
	private $_name;
	private $_userName;
	private $_email;
	private $_password;

	public function insert($options = []) {
		$this->db->insert('users', $options);
	}

	public function truncate()
	{
		$this->db->truncate('users');
	}

	public function get()
	{
		$query = $this->db->get('users');
		return $query->result();
	}

	public function setUserID($userID) {
	    $this->_userID = $userID;
	}  
	
	public function setEmail($email) {
	    $this->_email = $email;
	}

	public function setPassword($password) {
	    $this->_password = $password;
	}       
	 
	public function getUserInfo() {
	    $this->db->select(['u.id', 'u.name', 'u.email']);
	    $this->db->from('users as u');
	    $this->db->where('u.id', $this->_userID);
	    $query = $this->db->get();
	    return $query->row_array();
	}

	function login() {
	    $this ->db->select('id, name, email, role');
	    $this ->db->from('users');
	    $this ->db->where('email', $this->_email);
	    $this ->db->where('password', $this->_password);
	    $this ->db->limit(1);
	    $query = $this->db->get();
	    if($query -> num_rows() == 1) {
	      return $query->result();
	    } else {
	      return false;
	    }
	}    
}