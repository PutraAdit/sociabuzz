<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Order_model extends CI_Model {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    public function insert($data) {
        return $this->db->insert('orders', $data);
    }

    public function insert_detail($data) {
        return $this->db->insert('order_details', $data);
    }

    public function getData() {
        $this->db->select('o.id AS id, o.order_code AS order_code, o.total_order AS total, o.status AS stat, u.name AS uname, t.table_no AS table');
        $this->db->from('orders o'); 
        $this->db->join('users u', 'u.id = o.user_id', 'left');
        $this->db->join('tables t', 't.id = o.table_id', 'left');
        $this->db->order_by('o.id','desc');         
        $query = $this->db->get(); 
        if($query->num_rows() != 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getTables()
    {
        $query = $this->db->get('tables');
        return $query->result();
    }

    public function getTablesAvail()
    {
        $query = $this->db->where('status is null or status = true')->get('tables');
        return $query->result();
    }

    public function getUsers()
    {
        $query = $this->db->get('users');
        return $query->result();
    }

    public function getMenus()
    {
        $query = $this->db->where('status is null or status = true')->get('items');
        return $query->result();
    }

    public function getDetail($id)
    {
        $query = $this->db->select('i.name')
            ->from('order_details od')
            ->join('items i', 'i.id = od.item_id')
            ->where('od.order_id', $id)
            ->get();
        return $query->result_array();
    }

    public function viewData($wh) {
        $this->db->where($wh);
        return $this->db->get('orders')->result();
    }

    public function show($wh) {
        $this->db->select('o.id AS id, o.order_code AS order_code, o.total_order AS total, o.status AS stat, u.name AS uname, t.table_no AS table');
        $this->db->from('orders o'); 
        $this->db->join('users u', 'u.id = o.user_id', 'left');
        $this->db->join('tables t', 't.id = o.table_id', 'left');
        $this->db->where('o.id', $wh);
        $query = $this->db->get(); 
        if($query->num_rows() != 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function update($wh, $d_update) {
        $this->db->where($wh);
        return $this->db->update('orders', $d_update);
    }

    public function update_detail($wh, $d_update) {
        $this->db->where($wh);
        return $this->db->update('order_details', $d_update);
    }

    public function hapus($kondisi) {
        $this->db->where($kondisi);
        return $this->db->delete('orders');
    }

    public static function getCode()
    {
        $digits_needed = 7;
        $code_generate = '';
        $count = 0;

        while ( $count < $digits_needed ) {
            $random = mt_rand(0, 9);
            $code_generate .= $random;
            $count++;
        }

        return $code_generate;
    }

    public function getDataOwn($id) {
        $this->db->select('o.id AS id, o.order_code AS order_code, o.total_order AS total, o.status AS stat, u.name AS uname, t.table_no AS table');
        $this->db->from('orders o'); 
        $this->db->join('users u', 'u.id = o.user_id', 'left');
        $this->db->join('tables t', 't.id = o.table_id', 'left');
        $this->db->where('o.user_id', $id);
        $this->db->order_by('o.id','desc');         
        $query = $this->db->get(); 
        if($query->num_rows() != 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}