<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_model extends CI_Model {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

   	public function activity_insert($a, $b, $c)
    {
        $dataInput = [
        	'username' => $a,
        	'action_date' => $b,
        	'params_action' => $c
        ];

        $this->db->insert('activities', $dataInput);
    }

    public function getDataOwn($us)
    {
        $query = $this->db->where('username', $us)->get('activities');
        return $query->result();
    }

    public function getData()
    {
        $query = $this->db->get('activities');
        return $query->result();
    } 
}