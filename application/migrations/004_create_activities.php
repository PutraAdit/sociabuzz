<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_activities extends CI_Migration {

    public function up()
    {
        $this->load->dbforge();
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'action_date' => [
                'type' => 'DATETIME'
            ],
            'params_action' => [
                'type' => 'VARCHAR',
                'constraint' => '250'
            ],
            'created_at timestamp default current_timestamp',
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
                'on update' => 'NOW()'
            ] 
        ]);

        $this->dbforge->add_key('id', TRUE);  
        $this->dbforge->create_table('activities');
    }
}