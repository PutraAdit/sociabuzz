<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_tables extends CI_Migration {

    public function up()
    {
        $this->load->dbforge();
        $fields = [
            'status' => [
                'type' => 'BOOLEAN',
                'default' => true
            ],
        ];
        $this->dbforge->add_column('tables', $fields);
    }

    public function down()
    {
        $this->load->dbforge();  
        $this->dbforge->drop_column('tables', 'status');
    }
}