<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_column extends CI_Migration {

    public function up()
    {
        $this->load->dbforge();
        $fields = [
            'role' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
        ];
        $this->dbforge->add_column('users', $fields);
    }

    public function down()
    {
        $this->load->dbforge();  
        $this->dbforge->drop_column('users', 'role');
    }
}