<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_user extends CI_Migration {

    public function up()
    {
        $this->load->dbforge();
        $fields = [
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '100'
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => '50'
            ]
        ];
        $this->dbforge->add_column('users', $fields);
    }

    public function down()
    {
        $this->load->dbforge();  
        $this->dbforge->drop_column('users', 'username');
        $this->dbforge->drop_column('users', 'email');
        $this->dbforge->drop_column('users', 'password');
    }

}