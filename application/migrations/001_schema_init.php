<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Schema_init extends CI_Migration {

    public function up()
    {
        $this->load->dbforge();
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'created_at timestamp default current_timestamp',
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
                'on update' => 'NOW()'
            ] 
        ]);

        $this->dbforge->add_key('id', TRUE);  
        $this->dbforge->create_table('users');

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'price' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'status' => [
                'type' => 'BOOLEAN'
            ],
            'created_at timestamp default current_timestamp',
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
                'on update' => 'NOW()'
            ]
        ]);

        $this->dbforge->add_key('id', TRUE);  
        $this->dbforge->create_table('items');

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'order_code' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'total_order' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'status' => [
                'type' => 'BOOLEAN'
            ],
            'table_id' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'created_at timestamp default current_timestamp',
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
                'on update' => 'NOW()'
            ]
        ]);

        $this->dbforge->add_key('id', TRUE);  
        $this->dbforge->create_table('orders');

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'order_id' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'item_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'created_at timestamp default current_timestamp',
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
                'on update' => 'NOW()'
            ]
        ]);

        $this->dbforge->add_key('id', TRUE);  
        $this->dbforge->create_table('order_details');

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'table_no' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'created_at timestamp default current_timestamp',
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
                'on update' => 'NOW()'
            ]
        ]);

        $this->dbforge->add_key('id', TRUE);  
        $this->dbforge->create_table('tables');
    }

    public function down()
    {
        $this->load->dbforge();  
        $this->dbforge->drop_table('users');
        $this->dbforge->drop_table('items');
        $this->dbforge->drop_table('orders');
        $this->dbforge->drop_table('order_details');
        $this->dbforge->drop_table('tables');
    }

}