<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Activity extends CI_Controller {
 
    public function __construct() {
        parent:: __construct();
        $this->load->database();
        $this->load->model('activity_model');
        $this->load->helper('url');
        if($this->session->userdata('status') != "login"){
            redirect(base_url());
        }
    }

    public function index() {
        $layout['data'] = $this->activity_model->getData();
        $layout['activity'] = 'activities/data';
        $date = date('Y-m-d H:i:s');
        $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Visit activity/dashboard");
        $this->load->view('activities/index', $layout);
    }

    public function export_excel() {
        $data = [
            'title' => 'Laporan',
            'data' => $this->activity_model->getDataOwn($this->session->userdata('name'))
        ];
 
        $this->load->view('activities/laporan_excel', $data);
    }
}