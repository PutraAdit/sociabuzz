<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Order extends CI_Controller {
 
    public function __construct() {
        parent:: __construct();
        $this->load->database();
        $this->load->model('order_model');
        $this->load->model('activity_model');
        $this->load->helper('url');
        if($this->session->userdata('status') != "login"){
            redirect(base_url());
        }
    }

    public function index() {
        $layout['data'] = $this->order_model->getData();
        $layout['order'] = 'orders/data';
        $date = date('Y-m-d H:i:s');
        $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Visit index/dashboard");
        $this->load->view('orders/index', $layout);
    }

    public function create() {
        $layout['data'] = $this->order_model->getTables();
        $layout['user'] = $this->order_model->getUsers();
        $layout['menu'] = $this->order_model->getMenus();
        $layout['code'] = $this->order_model->getCode();
        $date = date('Y-m-d H:i:s');
        $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Visit create page");
        $this->load->view('orders/form', $layout);
    }

    public function insert() {
        $od = $this->input->post('order_code');
        $status = false;
        $table = $this->input->post('table_id');
        $menu = $this->input->post('menu_id[]');
        $user = $this->input->post('user_id');
        $mp = $this->db->where_in('id', $menu)->get('items')->result_array();
        $tord = [];
        foreach ($mp as $key => $value) {
            $tord[] = $value['price'];
        }
        $total = array_sum($tord);
        $dataInput = [
            'order_code' => "ORD-" . $od,
            'total_order'   => $total,
            'status' => $status,
            'table_id' => $table,
            'user_id' => $user
        ];

        $table_update = $this->db->where('id', $table)->update('tables', ['status' => false]);
        $this->order_model->insert($dataInput);
        $ords = $this->db->select('id')->where('order_code', "ORD-" . $od)->get('orders')->row();
        foreach ($menu as $key => $val) {
            $detailOrder = [
                'order_id' => $ords->id,
                'item_id' => $val
            ];

            $this->order_model->insert_detail($detailOrder);
        }
        $date = date('Y-m-d H:i:s');
        $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Insert order with order code : ORD-'" . $od . "'");
        redirect('order');
    }

    public function edit() {
        if ($this->uri->segment(3) != null) {
            $id = $this->uri->segment(3);

            $where = ['id' => $id];
            $layout['data'] = $this->order_model->getTablesAvail();
            $layout['data2'] = $this->order_model->getTables();
            $layout['user'] = $this->order_model->getUsers();
            $layout['menu'] = $this->order_model->getMenus();
            $layout['status'] = [[0 => 'Belum Dibayar'], [1 => 'Lunas']];
            $details = $this->order_model->getDetail($id);
            $dt = [];
            foreach ($details as $key => $value) {
                $dt[] = $value['name'];
            }
            $layout['order_detail'] = implode(', ', $dt);
            $layout['data_order'] = $this->order_model->viewData($where);
            $layout['order'] = 'orders/form_edit';
            $date = date('Y-m-d H:i:s');
            $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Visit edit page");
            $this->load->view('orders/index', $layout);
        }
    }

    public function update() {
        $id = $this->input->post('id');
        $od = $this->input->post('order_code');
        $status = $this->input->post('status');
        $table = $this->input->post('table_id');
        $menu = $this->input->post('menu_id[]');
        $user = $this->input->post('user_id');
        $kon = ['id' => $id];
        $exis_order = $this->order_model->viewData($kon);
        $result = $exis_order[0]->total_order;

        if (isset($menu)) {
            $mp = $this->db->where_in('id', $menu)->get('items')->result_array();
            $tord = [];
            foreach ($mp as $key => $value) {
                $tord[] = $value['price'];
            }
            $total_arr = array_sum($tord);
            $total = $result + $total_arr;
        } else {
            $total = $result;
        }

        $du = [
            'order_code' => $od,
            'total_order'   => $total,
            'status' => $status,
            'table_id' => $table,
            'user_id' => $user
        ];
        if ($this->order_model->update($kon, $du)) {
            if ($status == true) {
                $table_update = $this->db->where('id', $table)->update('tables', ['status' => true]);
            }
            $ords = $this->db->select('id')->where('id', $id)->get('orders')->row();
            foreach ($menu as $key => $val) {
                $detailOrder = [
                    'order_id' => $ords->id,
                    'item_id' => $val
                ];
                $this->order_model->insert_detail($detailOrder);
            }
            echo "berhasil Mengubah data";
        } else {
            echo "gagal mengubah data";
        }
        $date = date('Y-m-d H:i:s');
        $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Update order with order code : '" . $od . "'");
        redirect('order');
    }

    public function show() {
        if ($this->uri->segment(3) != null) {
            $id = $this->uri->segment(3);
            $details = $this->order_model->getDetail($id);
            $dt = [];
            foreach ($details as $key => $value) {
                $dt[] = $value['name'];
            }
            $layout['order_detail'] = implode(', ', $dt);
            $layout['data_order'] = $this->order_model->show($id);
            $date = date('Y-m-d H:i:s');
            $this->activity_model->activity_insert($this->session->userdata('name'), $date, "View order with order ID : '" . $id . "'");
            $this->load->view('orders/show', $layout);
        }
    }

    public function payment() {

    }

    public function hapus() {
        if ($this->uri->segment(3) != null) {
            $id = $this->uri->segment(3);
            $kon = ['id' => $id];

            if ($this->order_model->hapus($kon)) {
                $date = date('Y-m-d H:i:s');
                $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Delete order with order ID : '" . $id . "'");
                echo "berhasil Menghapus data";
            } else {
                echo "gagal menghapus data";
            }
        }

        redirect('order');
    }

    public function export_excel() {
        $data = [
            'title' => 'Order',
            'data' => $this->order_model->getDataOwn($this->session->userdata('id'))
        ];
 
        $this->load->view('orders/laporan_excel', $data);
    }

    public function update_table()
    {

        $id = $this->input->post('id');       
        $data = [
            'table_id' => $this->input->post('table_id')
        ]; 
        if (!$this->db->where('id', $id)->update('orders', $data)) {
            $hasError = $this->db->_error_message();
            echo $hasError;
        } else {
            redirect('order', 'refresh');
        }
    } 
}