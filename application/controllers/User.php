<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->faker = Faker\Factory::create(); 		
        $this->load->model('user_model');
        $this->load->model('activity_model');
    }

    public function seed()
    {
        $this->_truncate_db();
        $this->_seed_users(5); 
    }

    public function _seed_users($limit)
    {     
        for ($i = 0; $i < $limit; $i++) {          
            $data = [
                'username' => $this->faker->unique()->userName,
                'password' => md5('12345'),
                'name' => $this->faker->firstName,
                'email' => $this->faker->email,
            ];
 
            $this->user_model->insert($data);
        }
 		$this->session->set_flashdata('message', 'Database Seeds Successfully 5 Records Added In Database');
        redirect('user/index', 'location');
    }
 
    private function _truncate_db()
    {
        $this->user_model->truncate();
    }

    public function index()
    {
        $this->user_model->setUserID($this->session->userdata('id'));
        $data['userInfo'] = $this->user_model->getUserInfo();
        $this->load->view('users/dashboard', $data);
    }

    public function login()
    {
        $data['title'] = 'Login - Socia Test';
        $this->load->view('users/login', $data);
    }

    function doLogin() 
    {
        $this->form_validation->set_rules('email', 'Your Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
 
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('users/login');
        } else {  
            $sessArray = [];
            $email = $this->input->post('email');
            $password = $this->input->post('password');
 
            $this->user_model->setEmail($email);
            $this->user_model->setPassword(MD5($password));
 
            $result = $this->user_model->login();
            if ($result) {
                foreach($result as $row) {
                    $sessArray = [
                        'id' => $row->id,
                        'name' => $row->name,
                        'email' => $row->email,
                        'role' => $row->role,
                        'status' => 'login'
                    ];
                    $this->session->set_userdata($sessArray);
                }
                $date = date('Y-m-d H:i:s');
                $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Login to Dashboard");
                redirect('order');
            } else {
                redirect('user/login?msg=1');
            }  
        }
    }

    public function logout() {
        $this->session->unset_userdata('id');
        $date = date('Y-m-d H:i:s');
        $this->activity_model->activity_insert($this->session->userdata('name'), $date, "Logout from Application");
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('status');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('user/login');
    }
}
